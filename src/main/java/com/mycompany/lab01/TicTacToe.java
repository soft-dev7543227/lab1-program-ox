/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab01;

import java.util.Scanner;

public class TicTacToe {

    private char[][] board;
    private char currentPlayer;

    public TicTacToe(char startingPlayer) {
        board = new char[3][3];
        currentPlayer = startingPlayer;
        initializeBoard();
    }

    public void initializeBoard() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                board[row][col] = ' ';
            }
        }
    }

    public void displayBoard() {
        System.out.println("-------------");
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                System.out.print("| " + board[row][col] + " ");
            }
            System.out.println("|\n-------------");
        }
    }

    public void getPlayerInput() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Player " + currentPlayer + ", enter row (1-3): ");
        int row = sc.nextInt() - 1;
        System.out.print("Player " + currentPlayer + ", enter column (1-3): ");
        int col = sc.nextInt() - 1;

        makeMove(row, col);
    }

    public void makeMove(int row, int col) {
        if (board[row][col] == ' ') {
            board[row][col] = currentPlayer;
            currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
        } else {
            System.out.println("Invalid move. Try again.");
            getPlayerInput();
        }
    }

    public boolean checkForWinner() {
        // Check rows for a win
        for (int row = 0; row < 3; row++) {
            if (board[row][0] == board[row][1] && board[row][1] == board[row][2] && board[row][0] != ' ') {
                return true;
            }
        }

        // Check columns for a win
        for (int col = 0; col < 3; col++) {
            if (board[0][col] == board[1][col] && board[1][col] == board[2][col] && board[0][col] != ' ') {
                return true;
            }
        }

        // Check diagonals for a win
        if ((board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != ' ')
                || (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] != ' ')) {
            return true;
        }

        return false;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char symbol1, symbol2;
        int rounds = 1;
        boolean playAgain = true;

        while (playAgain) {
            System.out.println("Round " + rounds + ":");
            System.out.print("Player 1, choose your symbol (X/O): ");
            symbol1 = sc.nextLine().toUpperCase().charAt(0);

            symbol2 = (symbol1 == 'X') ? 'O' : 'X';

            TicTacToe game = new TicTacToe(symbol1);
            game.displayBoard();

            while (!game.checkForWinner()) {
                game.getPlayerInput();
                game.displayBoard();
            }

            System.out.println("Player " + game.currentPlayer + " wins!");

            System.out.print("Play again? (Y/N): ");
            String choice = sc.nextLine().toUpperCase();
            if (choice.equals("N")) {
                System.out.println("Bye Bye See you na kub");
                playAgain = false;
            } else {
                game.initializeBoard();
                rounds++;
                System.out.println();
            }
        }
    }
}
